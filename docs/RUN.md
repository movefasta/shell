# How to Run Orion by Siderus

To manually run Orion from source code you need to use GNU/Linux or macOS.
Windows development is not fully supported but it can be acheived.

To build Orion you need the following dependencies installed locally:

* [NodeJS](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com)
* [GNU Make](https://www.gnu.org/software/make/)
* [JQ](https://stedolan.github.io/jq/)
* [cURL](https://curl.haxx.se/)

Once everythig is available, you can run from a bash shell the
following command:

```shell
make clean run
```

This will download the depndencies, including IPFS binaries then start Orion.