import rootDir from 'app-root-dir'
import pjson from '../../../package.json'
import { getIpfsApiWithAddress } from '../ipfs/api'
import { addFileLocallyFromPath } from '../ipfs/files'
import { getNetworkPeers } from './peers'
import { connectTo, addBootstrapAddr } from '../ipfs/peers'
import { promiseSleep } from '../utils'
import isOnline from 'is-online'
import log from 'electron-log'
import tar from 'tar'
import { dirSync as tmpDirSync } from 'tmp'
import { createReadStream, copyFileSync } from 'fs'

/**
 * getLatestUIPath will provide the latest UI version or if not possible, the
 * current supported.
 *
 * @returns {Promise<string>}
 */
export function getLatestUIPath () {
  return isOnline().then((isConn) => {
    if (!isConn) {
      return Promise.resolve(pjson.ui.cid)
    }

    return getIpfsApiWithAddress(global.IPFS_MULTIADDR_API)
      .then(ipfs_client => ipfs_client.dns(pjson.ui.ipns))
      .then(cid => {
        // if the new CID is not available return the default one
        if (!cid) return Promise.resolve(pjson.ui.cid)
        return Promise.resolve(cid)
      })
  })
}

/**
 * preloadUICID will fetch a specific DAG for the UI. This should speed up
 * the download of the content
 *
 * @returns {Promise<string>}
 */
export function preloadUICID (cid) {
  return isOnline().then((isConn) => {
    if (!isConn) {
      return Promise.resolve()
    }

    return getIpfsApiWithAddress(global.IPFS_MULTIADDR_API)
      .then(ipfs_client => ipfs_client.dag.get(cid))
      .then(() => Promise.resolve()) // Clean the output
  })
}

/**
 * addIncludedUI is designed to add to the UI that is included in the app
 * to the local node in order to offer offline interface.
 */
export function addIncludedUI () {
  // the UI pkg has to be copied out from ASAR package
  const uipkgPath = `${rootDir.get()}/assets/ui.tar`
  const uiDirPath = tmpDirSync({ keep: true }).name
  const tmpPkgPath = `${uiDirPath}/ui.tar`
  const destUIPath = tmpDirSync({ keep: true }).name

  log.debug(`[App] Decompressing UI from ${uipkgPath}, to ${destUIPath}`)
  copyFileSync(uipkgPath, tmpPkgPath)

  return tar.extract({ cwd: destUIPath, file: tmpPkgPath }).then(() => {
    log.debug(`[App] Done decompressing`)

    const options = { recursive: true, pin: false, wrapWithDirectory: false }
    return addFileLocallyFromPath(`${destUIPath}`, options)
      .then((result) => {
        log.debug(`[App] Added UI in IPFS: ${result[result.length - 1].hash}`)
      })
  })
}

// loadLatestUI is a chain of promises to ensure connection and preload the
// UI from the latest version available via IPNS/DNSLink
export function loadLatestUI (loadingWindow) {
  log.debug('[App] Fetching edge nodes of Siderus Network')
  // Fetch the peers to connect to
  loadingWindow.webContents.send('set-progress', {
    text: 'Fetching data from Siderus Network', percentage: 50
  })

  return getNetworkPeers()
    .catch(err => {
      log.warn('[App] Error while fetching the Siderus nodes: ', err)
      return Promise.resolve([])
    })
    .then(peers => {
      log.debug('[App] Connecting to Siderus Network')
      // Connect to the peers
      loadingWindow.webContents.send('set-progress', {
        text: 'Connecting to Siderus Network', percentage: 55
      })
      const connectPromises = peers.map(addr => { return connectTo(addr, global.IPFS_MULTIADDR_API) })
      const bootstrapPromises = peers.map(addr => { return addBootstrapAddr(addr, global.IPFS_MULTIADDR_API) })
      return Promise.all(connectPromises.concat(bootstrapPromises))
        .catch(err => {
          log.warn('[App] Error while connecting to Siderus peers: ', err)
          return Promise.resolve()
        })
    })
    // Sleep 1 second due to connections to the network
    .then(() => promiseSleep(1 * 1000))
    // Download/pre-fetch latest SPA.
    .then(() => {
      loadingWindow.webContents.send('set-progress', {
        text: 'Fetching latest interface version', percentage: 70
      })
      log.debug('[App] Fetching CID for latest SPA')
    })
    // Downloads the SPA interaface to ensure the window is not empty
    .then(getLatestUIPath)
    .then((cid) => {
      log.info('[App] Latest SPA version:', cid)
      loadingWindow.webContents.send('set-progress', {
        text: 'Fetching latest interface content from IPFS', percentage: 80
      })
      log.debug('[App] Fetching Dag for:', cid)
      return preloadUICID(cid)
    })
}
