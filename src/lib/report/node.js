const pjson = require('../../../package.json')
const { init } = require('@sentry/electron')

init({
  dsn: 'https://16b4a9a202594aa99b8419af9dec7b5c@sentry.io/1339053',
  release: `${pjson.version}/IPFS-${pjson.ipfsVersion}`,
  environment: `${pjson.env}`
})
