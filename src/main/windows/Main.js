import path from 'path'
import { BrowserWindow } from 'electron'
import log from 'electron-log'

export default function create (app) {
  log.debug('[MainWindow] Creating')

  // Create the browser window.
  let theWindow = new BrowserWindow({
    width: 1024,
    height: 750,
    minWidth: 800,
    minHeight: 650,
    frame: false,
    // MacOS option to show native buttons in frameless:
    titleBarStyle: 'hiddenInset',
    fullscreenable: true,
    icon: path.join(__dirname, '../../../assets/logo.png'),
    show: false,
    webPreferences: {
      // allow electron in the spa
      nodeIntegration: true,
      // allow node modules in the web worker
      nodeIntegrationInWorker: true,
      preload: path.join(__dirname, '../preload.js')
    }
  })

  // Clear the Cache (ensure we rely on IPFS instead)
  theWindow.webContents.session.clearCache((...args) => {
    log.debug('[MainWindow] Cache Cleared')
    console.debug('[Window] Cache Cleared', ...args)
  })

  theWindow.loadURL(global.UI_HTTP_ADDRESS)

  theWindow.once('ready-to-show', () => {
    log.debug('[MainWindow] Ready to show')
    theWindow.show()
    theWindow.focus()
  })

  theWindow.on('close', (e) => {
    log.debug('[MainWindow] Closing')
    // Prevent from closing when the app is still active. This will
    // make the window load fater and prevents activities from failign
    e.preventDefault()
    theWindow.hide()
  })

  app.on('before-quit', () => {
    // This will make sure that the app will quit. If not done it will
    // wait for the window to "close" but the listner is preventing that.
    theWindow.removeAllListeners('close')
  })

  // Emitted when the window is closed.
  theWindow.on('closed', () => {
    log.debug('[MainWindow] Closed')
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    if (app !== undefined && app.mainWindow !== null) {
      app.mainWindow = null
    }
    theWindow = null
  })

  log.debug('[MainWindow] Created')
  return theWindow
}
